export default [
    {
        title: 'Inicio',
        url:'/'
    },
    {
        title: 'Personal Administrativo',
        url:'/AdministrativeStaff'
    },
    {
        title: 'Clientes',
        url:'/Client'
    },
    {
        title: 'Productos',
        url:'/Products'
    }
];