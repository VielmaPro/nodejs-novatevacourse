const express = require('express');
const router = express.Router();

const Client = require('../models/Client');

router.get('/get' , async (req, res) => 
{
    const Clients = await Client.find();
    console.log('received');
    res.json(Clients);
});


router.get('/get/:id' , async(req , res) =>
{
    const ClientD = await Client.findById(req.params.id);
    if(!(ClientD))
    res.json({status: 'Not Found'});
    else
    res.json(ClientD);
});

router.post('/post' , async (req, res) =>
{
    const{ name,ID,age,gender,birthdate,adress} = req.body;
    const clients = new Client({name,ID,age,gender,birthdate,adress});
    if(Client.ID == clients.ID)
    res.json({status:'Client registred'});
    else
    {
        await clients.save();
    res.json({status:'Client saved'});
    }
});

router.put('/put/:id' , async(req , res) =>
{
    const{ name,ID,age,gender,birthdate,adress} = req.body;
    const newClient = { name,ID,age,gender,birthdate,adress};
    const aux = await Client.findByIdAndUpdate(req.params.id, newClient);
    if(aux)
    res.json({status:'Client Updated'});
    else
    res.json({status:'Client Not found'});
    
});


router.delete('/delete/:id' , async(req , res) =>
{
    const aux = await Client.findByIdAndDelete(req.params.id);
    if(aux)
    res.json({status:'Client deleted'});
    else
    res.json({status:'Client Not found'});
});


module.exports = router;    