const express = require('express');
const router = express.Router();

const bodyParser = require('body-parser');

const urlencodedParser = bodyParser.urlencoded({ extended: false });

const AdministrativeStaff = require('../models/AdministrativeStaff');

router.get('/get' , async (req, res) => 
{
    const AdministrativeStaffs = await AdministrativeStaff.find();
    res.json(AdministrativeStaffs);
});


router.get('/get/:id' , async(req , res) =>
{
    const AdministrativeStaffD = await AdministrativeStaff.findById(req.params.id);
    if(!(AdministrativeStaffD))
    res.json({status: 'Not Found'});
    else
    res.json(AdministrativeStaffD);
});

router.get('/get/:name' , async(req , res) =>
{
    const AdministrativeStaffD = await AdministrativeStaff.findBy(req.params.name);
    if(!(AdministrativeStaffD))
    res.json({status: 'Not Found'});
    else
    res.json(AdministrativeStaffD);
});


router.post('/post' , urlencodedParser, async(req, res) =>
{
    const{ name,ID,age,gender,birthdate,adress,username,password,email} = req.body;
    const AdministrativeStaffs = new AdministrativeStaff({name,ID,age,gender,birthdate,adress,username,password,email});
    await AdministrativeStaffs.save();
    res.json({status:'AdministrativeStaff saved'});
});

router.put('/put/:id' , async(req , res) =>
{
    const{ name,ID,age,gender,birthdate,adress,username,password,email} = req.body;
    const newAdministrativeStaff = { name,ID,age,gender,birthdate,adress,username,password,email};
    const aux = await AdministrativeStaff.findByIdAndUpdate
    (req.params.id, newAdministrativeStaff);
    
    if(aux)
    res.json({status:'AdministrativeStaff Updated'});
    else
    res.json({status:'AdministrativeStaff Not found'});
});


router.delete('/delete/:id' , async(req , res) =>
{
    const aux = await AdministrativeStaff.findByIdAndDelete(req.params.id);
    if(aux)
    res.json({status:'AdministrativeStaff deleted'});
    else
    res.json({status:'AdministrativeStaff Not found'});
});

module.exports = router;    