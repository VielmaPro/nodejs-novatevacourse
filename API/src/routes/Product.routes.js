const express = require('express');
const router = express.Router();

const Product = require('../models/Product');

router.get('/get' , async (req, res) => 
{
    const Products = await Product.find();
    console.log('received');
    res.json(Products);
});


router.get('/get/:id' , async(req , res) =>
{
    const ProductD = await Product.findById(req.params.id);
    if(!(ProductD)) 
    res.json({status: 'Not Found'});
    else
    res.json(ProductD);
});

router.post('/post' , async (req, res) =>
{
    const{ name,quantityPerKg,category,price} = req.body;
    const Products = new Product({name,quantityPerKg,category,price});
    await Products.save();
    res.json({status:'Product saved'});
});

router.put('/put/:id' , async(req , res) =>
{
    const{ name,quantityPerKg,category,price} = req.body;
    const newProduct = { name,quantityPerKg,category,price};
    const aux = await Product.findByIdAndUpdate(req.params.id, newProduct);
    if(aux)
    res.json({status:'Product Updated'});
    else
    res.json({status:'Product Not found'});
});


router.delete('/delete/:id' , async(req , res) =>
{
    const aux = await Product.findByIdAndDelete(req.params.id);
    if(aux)
    res.json({status:'Product deleted'});
    else
    res.json({status:'Product Not found'});
});


module.exports = router;    