//Dependencies
import express from 'express';
import webpack from 'webpack';
import path from 'path';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import open from 'open';

//webpack configuration
import webpackConfig from '../webpack.config'

//API
import AdministrativeStaff from './routes/AdministrativeStaff.routes';
import Client from './routes/Client.routes';
import Product from './routes/Product.routes';

//Enviroment
const isDevelopment = process.env.NODE_ENV !== 'production';

//server port
const port = 3000;

//express app
const app = express();

//webpack compiler
const webpackCompiler = webpack(webpackConfig);

if (isDevelopment){
    app.use(webpackDevMiddleware(webpackCompiler));
    app.use(webpackHotMiddleware(webpackCompiler));
}

//API dispatch
app.use('/AdministrativeStaff', AdministrativeStaff);
app.use('/Client', Client);
app.use('/Product', Product);

//Sending all traffic to React
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/index.html'));
});

//Listen port
app.listen(port, err =>{
    if(!err)
    {
        open(`http://localhost:${port}`);
    }
});

/*const express = require('express');
const app = express();
const morgan = require('morgan');
const path = require('path');
const { mongoose } = require('./database');

//Settings
app.set('port', process.env.PORT || 3000);

//Middlewares
app.use(morgan('dev'));
app.use(express.json());

//Static Files
app.use(express.static(path.join(__dirname, 'public')));

//routes
app.use('/api/Client',require('./routes/Client.routes'));
app.use('/api/Product',require('./routes/Product.routes'));
app.use('/api/AdministrativeStaff',require('./routes/AdministrativeStaff.routes'));

app.listen(app.get('port'), () =>
{
 console.log(`Server on port ${app.get('port')}`);
});*/