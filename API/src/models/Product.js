const mongoose = require('mongoose');
const { Schema } = mongoose;

const DataProduct = new Schema(
    {
        name : { type: String , required: true},
        quantityPerKg: { type: Number , required: true},
        category : { type: String , required: true},
        price : { type: Number , required: true}
    });
    module.exports = mongoose.model('Product',DataProduct);