const mongoose = require('mongoose');
const { Schema } = mongoose;

const DataClient = new Schema(
    {
        name : { type: String , required: true},
        ID: { type: String , required: true},
        age : { type: Number , required: true},
        gender : { type: String , required: true},
        birthdate: { type: Date , required: true},
        address: { type: String , required: true}

    });
    module.exports = mongoose.model('Client',DataClient);

