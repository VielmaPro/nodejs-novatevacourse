//Dependencies
import React from 'react';
import {Route , Switch} from 'react-router-dom';

//Components
import App from './Components/App';
import Home from './Components/Home';
import Client from './Components/Client';
import AdministrativeStaff from './Components/AdministrativeStaff';
import Products from './Components/Products';
import Page404 from './Components/Page404';

const AppRoutes = () => 
    <App>
        <Switch>
            <Route exact path="/Client" component={Client}/>
            <Route exact path="/AdministrativeStaff" component={AdministrativeStaff}/>
            <Route exact path="/Products" component={Products}/>
            <Route exact path="/" component={Home}/>
            <Route component={Page404}/>
        </Switch>
    </App>;

export default AppRoutes;