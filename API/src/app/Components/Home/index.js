//Dependencies
import React, {Component} from 'react';

class Home extends Component
{
    render()
    {
        return(
                <div className="row">
                    <div className="container">
                    <div className="col s12 m4">
                        <div className="card blue-grey darken-1">
                            <div className="card-content white-text">
                            <span className="card-title">Personal Administrativo</span>
                                <p>Aqui encontraras personal activo en la empresa, como tambien
                                    podras agregar, eliminar o modificar alguno de ellos.
                                </p>
                        </div>
                    </div>
                </div>
            <div className="col s14 m4">
                <div className="card blue-grey darken-1">
                    <div className="card-content white-text">
                    <span className="card-title">Clientes</span>
                        <p>Aqui encontraras Clientes de la empresa, como tambien
                            podras agregar, eliminar o modificar alguno de ellos.
                        </p>
                </div>
            </div>
        </div>
        <div className="col s12 m4">
                <div className="card blue-grey darken-1">
                    <div className="card-content white-text">
                    <span className="card-title">Productos</span>
                        <p>Aqui encontraras Productos registrados en la base de datos de la empresa, como tambien
                            podras agregar, eliminar o modificar alguno de ellos.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
        );
    }
}

export default Home;