//Dependencies
import React, {Component} from 'react';

//assets
class Footer extends Component
{
 render()
    {
        return(
            <footer className="page-footer" style={{background: '#222'}}>
            <div className="footer-copyright">
              <div className="container">
              <a className="grey-text text-lighten-1 right">© 2019 ViBo Studios</a>
              </div>
            </div>
          </footer>
              
        )
    }
}

export default Footer;