//Dependencies
import React, {Component} from 'react';

class AdministrativeStaff extends Component
{
  constructor()
  {
    super();
    this.state = {
        name : "",
        ID: "",
        age :"",
        gender :"",
        birthdate: "",
        address: "",
        username: "",
        password: "",
        email: ""
    };
    this.addStaff = this.addStaff.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  addStaff(e)
  {
    fetch('/AdministrativeStaff/post', {
      method:'POST',
      body: JSON.stringify(this.state),
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(data => {
        console.log(data)
        M.toast({html:'Saved'})
      })
      .catch(err => console.error(err)); 
    e.preventDefault();
  }

  componentDidMount(){
    this.fetchStaff();
  }

  fetchStaff(){
    fetch('/AdministrativeStaff/get')
        .then(res => res.json())
        .then(data => console.log(data));
  }

  handleChange(e)
  {
    const {name, value} = e.target;
    this.setState({
      [name]: value
    });
  }

    render()
    {
        return(
            <div className="container">
                <div className="row">
    <form onSubmit={this.addStaff}>
      <div className="row">
        <div className="input-field col s6">
        <i className="material-icons prefix">account_circle</i>
          <input id="name" placeholder="Nombre y apellido" onChange={this.handleChange} name="name" type="text" className="validate"/>
          <label htmlFor="name">Nombre y apellido</label>
        </div>
        <div className="input-field col s6">
          <input name="ID" type="text" onChange={this.handleChange} className="validate"/>
          <label htmlFor="ID">C.I</label>
        </div>
        </div>
        <div className="input-field col s6">
          <input name="age" type="text" onChange={this.handleChange} className="validate"/>
          <label htmlFor="age">Edad</label>
        </div>
        <div className="input-field col s6">
          <input name="gender" type="text" onChange={this.handleChange} className="validate"/>
          <label htmlFor="gender">Sexo</label>
        </div>
        <div className="input-field col s6">
          <input name="birthdate" type="text" onChange={this.handleChange} className="validate"/>
          <label htmlFor="birthdate">Fecha</label>
        </div>
        <div className="input-field col s6">
          <input name="address" type="text" onChange={this.handleChange} className="validate"/>
          <label htmlFor="address">Direccion</label>
        </div>
        <div className="input-field col s6">
          <input name="username" type="text" onChange={this.handleChange} className="validate"/>
          <label htmlFor="username">Usuario</label>
        </div>
        <div className="input-field col s6">
          <input name="password" type="text" onChange={this.handleChange} className="validate"/>
          <label htmlFor="password">Contrasena</label>
        </div>
        <div className="input-field col s6">
          <input name="email" type="text" onChange={this.handleChange} className="validate"/>
          <label htmlFor="email">Email</label>
        </div>
        <button type="submit" className="btn grey darken-4">
          Enviar
        </button>
    </form>
  </div>
            </div>
        );
    }
}

export default AdministrativeStaff;