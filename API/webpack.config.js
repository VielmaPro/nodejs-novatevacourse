//Dependencies
import webpack from 'webpack';
import path from 'path';

//paths
const PATHS = {
    index: path.join(__dirname, ' ./src/app/index'),
    build: path.join(__dirname, 'src/public'),
    base: path.join(__dirname, 'src')
};

//Webpack Config
export default {
    mode: 'development',
    devtool: 'cheap-module-eval-source-map',
    entry:'./src/app/index',
        //'webpack-hot-middleware/client?reload=true',
    output: {
        path: PATHS.build,
        publicPath: '/',
        filename: 'bundle.js'
    },
    plugins:[
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
    ],
    devServer: {
        inline: false,
    },
    resolve: {
        extensions: ['.js', '.jsx']
      },
    module: {
        rules:[{
            test: /\.js?$/,
            loader: 'babel-loader',
            enforce: "pre", // preload the jshint loader
            include: PATHS.base
        },
        {
            test:/(\.css)$/,
            use: [
                {loader: 'style-loader'},
                {loader:'css-loader'}
            ]
        },
        {
            test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
            loader: 'url-loader?limit=100000' 
        }
       ]
    }
};